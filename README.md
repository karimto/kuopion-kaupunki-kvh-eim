# How to add a new Git project

Pay a visit to pso-eim-baseline repository, if you aren't there yet.

1. Click Actions -> Fork
2. Select "EIM configurations" as a target project
3. Add the name of the new repository (lower case letters)
4. Disable fork syncing
5. Click "Fork repository"
6. Start developing!

# After forking

Encrypt all ansible_vars files with a newly generated vault password and store the password in to Pleasant password server. Deploy the repository to a remote using Ansible.

# Modifications to repository settings
1. Prevent commits to the master by adding branch permission:  
-> Settings  
-> Branch permissions  
-> Add permissions  
-> Branches -> Select branch -> master  
-> Prevent: Checkmark "Rewriting history", "Changes without a pull request" and "Branch deletion"  
-> Create  
2. Prevent merging pull requests without approvers  
-> Settings  
-> Pull requests  
-> Checkmark "Requires 1 approvers"  
-> Save
3. Add Slack integration  
-> Settings  
-> Slack settings  
-> Checkmark "Override settings for global slack notification options"  
-> Checkmark "Enable slack notifications for pull requests"  
-> Checkmark "Enable slack notifications for personal"  
-> Channel name: #pso_git  
-> Webhook url: https://hooks.slack.com/services/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  
-> Save
